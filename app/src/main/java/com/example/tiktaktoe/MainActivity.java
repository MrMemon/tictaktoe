package com.example.tiktaktoe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

public class MainActivity extends AppCompatActivity {

    char player = 'x';

    Button btn11, btn12, btn13, btn21, btn22, btn23, btn31, btn32, btn33;
    TextView playername, winner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn11 = findViewById(R.id.btn_box11);
        btn12 = findViewById(R.id.btn_box12);
        btn13 = findViewById(R.id.btn_box13);
        btn21 = findViewById(R.id.btn_box21);
        btn22 = findViewById(R.id.btn_box22);
        btn23 = findViewById(R.id.btn_box23);
        btn31 = findViewById(R.id.btn_box31);
        btn32 = findViewById(R.id.btn_box32);
        btn33 = findViewById(R.id.btn_box33);

        playername = findViewById(R.id.tv_playername);
        winner = findViewById(R.id.tv_winner);

    }

    public void performAction(View view) {

        if (view.getId() == btn11.getId()) {
            btn11.setText(String.valueOf(player));
            btn11.setEnabled(false);

        } else if (view.getId() == btn12.getId()) {
            btn12.setText(String.valueOf(player));
            btn12.setEnabled(false);

        } else if (view.getId() == btn13.getId()) {
            btn13.setText(String.valueOf(player));
            btn13.setEnabled(false);

        } else if (view.getId() == btn21.getId()) {
            btn21.setText(String.valueOf(player));
            btn21.setEnabled(false);

        } else if (view.getId() == btn22.getId()) {
            btn22.setText(String.valueOf(player));
            btn22.setEnabled(false);

        } else if (view.getId() == btn23.getId()) {
            btn23.setText(String.valueOf(player));
            btn23.setEnabled(false);

        } else if (view.getId() == btn31.getId()) {
            btn31.setText(String.valueOf(player));
            btn31.setEnabled(false);

        } else if (view.getId() == btn32.getId()) {
            btn32.setText(String.valueOf(player));
            btn32.setEnabled(false);

        } else if (view.getId() == btn33.getId()) {
            btn33.setText(String.valueOf(player));
            btn33.setEnabled(false);

        }


        if (giveResult() == player)
            makeText(this, player + " has Won", LENGTH_SHORT).show();

        if (player == 'x') {
            player = 'o';
            playername.setText("Now playing: o");
        } else {
            player = 'x';
            playername.setText("Now playing: x");
        }

    }

    public char giveResult() {

        if (btn11.getText().toString().equals(btn12.getText().toString()) && btn11.getText().toString().equals(btn13.getText().toString()) && !btn11.getText().toString().isEmpty()
                || btn21.getText().toString().equals(btn22.getText().toString()) && btn21.getText().toString().equals(btn23.getText().toString()) && !btn21.getText().toString().isEmpty()
        ) {
            return player;
        } else return 'z';
    }

    //this comment is to verify that bitBucket is working
}
